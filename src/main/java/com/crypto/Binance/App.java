package com.crypto.Binance;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.swing.JOptionPane;

import org.apache.log4j.PropertyConfigurator;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;

/**
 * Hello world!
 *
 */
public class App 
{
	@SuppressWarnings("restriction")
	public static void main(String args[]) throws InterruptedException {
		PropertyConfigurator.configure("src/main/resources/log4j.properties");
		BinanceChecking binCheck = new BinanceChecking();
		binCheck.requestChecking();
	}
}
