package com.crypto.Binance;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.ZonedDateTime;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.control.Alert;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class BinanceChecking {

    static Logger log = Logger.getLogger(BinanceChecking.class.getName());

    public void requestChecking() {
        String original = requestBin();
        while(true) {
            try {
                Thread.sleep(3000);
                String laterReponse = requestBin();
                if(!original.equalsIgnoreCase(laterReponse)) {
                    System.out.println("~~~~~~~~~~~Content different~~~~~~~~~~~");
                    ZonedDateTime hour = ZonedDateTime.now();
                    System.out.println("Change deteced in: " + hour);
                    log.info("Change deteced in: " + hour);
                    System.out.println("~~~Original content: " + original);
                    log.info("~~~Original content: " + original);
                    System.out.println("~~~Changed content: " + laterReponse);
                    log.info("~~~Changed content: " + laterReponse);
                    System.out.println("-----------> Difference: " + StringUtils.difference(original,laterReponse));
                    log.info("-----------> Difference: " + StringUtils.difference(original,laterReponse));
                    new JFXPanel();
                    runMusic();
                    Platform.runLater(new Runnable() {
                        public void run() {
                            Alert alert = new Alert(Alert.AlertType.WARNING);
                            alert.setTitle("Money incoming");
                            alert.setHeaderText("Binance: ");
                            alert.setContentText("Content changed");
                            alert.showAndWait();
                        }
                    });
                    break;
                }
                else {
                    System.out.println("Contents are same!!!");
                    log.info("Contents are same!!!");
                }
            }
            catch(Exception e) {
                System.out.println("~~~~~~~~~~~Something goes wrong~~~~~~~~~~");
                System.out.println(e.getMessage());
                log.info("~~~~~~~~~~~Something goes wrong~~~~~~~~~~");
                log.info(e.getMessage());
            }

        }
    }

    private String requestBin() {
        try {
            URL url = new URL("https://support.binance.com/hc/en-us/sections/115000106672-New-Listings");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            conn.setRequestMethod("GET");
            conn.connect();

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String output;
            StringBuilder result = new StringBuilder();
            while ((output = br.readLine()) != null) {
                if(output.indexOf("article-list-link") > -1)
                    result.append(output);
            }

            conn.disconnect();

            return result.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "nothing";
    }

    private void runMusic() {
        String      bip         = "Billionaire.mp3";
        Media       hit         = new Media(new File(bip).toURI().toString());
        MediaPlayer mediaPlayer = new MediaPlayer(hit);
        mediaPlayer.play();
    }
}
